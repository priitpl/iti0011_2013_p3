package ee.ttu.cs.iti0011.iabb104231.k3.GUI;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

public class PaintingPanel extends Canvas
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1523860078249653351L;

	private boolean movingMode = false;
	
	/**
	 * Current shape.
	 */
	protected ShapeObject currentShape;
	
	/**
	 * Flag - show if we moving shape
	 */
	protected ShapeObject movingShape;
	
	/**
	 * Point buffer.
	 * Just in case, if need to buffer point.
	 */
	protected Point pointBuffer;
	
	/**
	 * The list of current shapes.
	 */
	protected List<ShapeObject> shapes = new ArrayList<ShapeObject>();
	
	/**
	 * History - here we store all snapshots of shapes lists.
	 * This is not the best solution, but fast to implement.
	 */
	protected List<List<ShapeObject>> history = new ArrayList<List<ShapeObject>>();
	
	/**
	 * History pointer - points to the history list.
	 * When history is empty, nowhere to point.
	 * When history is at least 1 action, in list it's 0 position
	 * so the historyStep pointer have to be 0.
	 */
	protected int historyStep = -1;
	
	/**
	 * 
	 */
	public static final double FINDER_MIN_DISTANCE = 30D;
	
	
	/**
	 * Main constructor.
	 */
	public PaintingPanel()
	{
		setBackground(new Color(255, 255, 255));
		
		// Create mouse and keyboard adapter.
		MouseAdapter mouseAdapter = new PainterMouseKeyAdapter();

		// add mouse and keyboard listeners.
		addMouseListener(mouseAdapter);
		addMouseMotionListener(mouseAdapter);
		addKeyListener((KeyListener)mouseAdapter);
		createHistorySnapshot();
	}
	
//	public void paintComponent(Graphics g)
	/**
	 * Method for repainting canvas
	 */
	public void paint(Graphics g)
	{
	    Iterator<ShapeObject> iterator = shapes.iterator();
	    while(iterator.hasNext()) 
	    	iterator.next().draw(g);
	}
	
	/**
	 * Find the closest one object to given point.
	 * @param point
	 * @return
	 */
	public ShapeObject findNearestObject(Point point)
	{
		Iterator<ShapeObject> iterator = shapes.iterator();
		double closestDistance = 999999999D;
		ShapeObject closestObject = null;
		while(iterator.hasNext())
	    {
			double dt;
	    	ShapeObject shape = iterator.next();
	    	if ((dt = shape.distanceTo(point)) < closestDistance) {
	    		closestDistance = dt;
	    		closestObject = shape;
	    	}
	    }
		return closestObject;
	}
	
	/**
	 * Create clone of the shapes.
	 * @return
	 */
	public List<ShapeObject> cloneShapes()
	{
		System.out.println("Cloning shapes.");
		
		List<ShapeObject> duplicate = new ArrayList<ShapeObject>();
		
		if (shapes.size() == 0) return duplicate;
		
		Iterator<ShapeObject> iterator = shapes.iterator();
		while(iterator.hasNext())
			duplicate.add(iterator.next().clone());
		
		return duplicate;
	}
	
	/**
	 * Change all shapes color to given one.
	 * @param c
	 */
	private void colorAllShapesToColor(Color c)
	{
		Iterator<ShapeObject> iterator = shapes.iterator();
		while(iterator.hasNext()) iterator.next().setColor(c);
	}
	
	/**
	 * Undo method.
	 */
	public void undo()
	{
		// if the latest step or size is 0 - ask to leave.
		if (historyStep <= 0 || history.size() == 0) {
			int i = JOptionPane.showConfirmDialog(this, "Do you want to exit?", "Exit?", JOptionPane.YES_NO_OPTION);
			if (i == JOptionPane.YES_OPTION) {
				System.exit(0);
			}
			return;
		}
		
		shapes = history.get(--historyStep);
		shapes = cloneShapes();
		repaint();
	}
	
	/**
	 * Redo method
	 */
	public void redo()
	{
		System.out.println("REDO()");
		if (historyStep + 1 >= history.size()) {
			System.out.println("It was the latest step from history.");
			return;
		}
		shapes = history.get(++historyStep);
		repaint();
	}
	
	/**
	 * Move given shape to given coordinate.
	 * @param p
	 */
	private void moveShape(Point p)
	{
		if (!movingMode || currentShape == null) {
			return;
		}
		if (pointBuffer == null) {
			pointBuffer = p;
			return;
		}
		
		currentShape.move(p.x - pointBuffer.x, p.y - pointBuffer.y);
		repaint();
		pointBuffer = p;
		
	}
	
	/**
	 * Create snapshot of current shapes.
	 */
	private void createHistorySnapshot()
	{
		System.out.println("CREATE SNAPSHOT");
		// Before creating snapshot, we have to clear 
		// all steps after current history pointer.
		while((historyStep + 1) < history.size()) {
			System.out.println("remove unnecceserry history stepst: stepst: " + history.size() + " remove:" + (history.size()-1));
			history.remove(history.size() - 1);
		}
		
		System.out.println("HISTORY: STARTIGN STATE BEFORE ADD: " + historyStep + " AND SIZE: " + history.size());
		history.add(cloneShapes());
		historyStep++;
		System.out.println("HISTORY: STARTIGN STATE AFTER ADD: " + historyStep + " AND SIZE: " + history.size());
	}

	/**
	 * Separate adapter for mouse and keyboard action listening.
	 * Done this way to reduce public methods for canvas.
	 * Given inner class is not visible to the world.
	 * @author priitpl
	 *
	 */
	protected class PainterMouseKeyAdapter extends MouseAdapter implements KeyListener
	{
		private boolean simpleLine = false;
		
		@Override
		public void keyTyped(KeyEvent e) {}

		@Override
		public void keyPressed(KeyEvent e)
		{	
			System.out.println("Key pressed: " + e.getKeyCode());
			switch(e.getKeyCode()) {
				case 17: movingMode = true; break;
				case 27: undo(); break;
			}
		}

		@Override
		public void keyReleased(KeyEvent e) 
		{
//			System.out.println("EVENT: " + e.toString());
			if (e.getKeyCode() == 17) {
				movingMode = false;
				currentShape = null;
				colorAllShapesToColor(Color.BLACK);
				repaint();
			}
		}
		
		@Override
		public void mouseDragged(MouseEvent event) 
		{
//			System.out.println("EVENT: mouseDragged " + event.toString());
			// If moving mode on and moving shape is set
			// Move given shape.
			if (movingMode && currentShape != null) {
				moveShape(event.getPoint());
				return;
			}
						
			if (!simpleLine && !movingMode) {
				if (currentShape == null) {
					currentShape = new ShapeObject();
					shapes.add(currentShape);
				}
				if (pointBuffer != null) {
					currentShape.addPoint(pointBuffer);
					pointBuffer = null;
				}
				currentShape.addPoint(event.getPoint());
				repaint();
			}
		}

		@Override
		public void mouseMoved(MouseEvent event) 
		{
			
			if (movingMode) {
				ShapeObject closest;
				colorAllShapesToColor(Color.BLACK);
				if (null != (closest = findNearestObject(event.getPoint()))){
					closest.setColor(Color.RED);
					currentShape = closest;
				}
				repaint();
			}
		}

		@Override
		public void mousePressed(MouseEvent event) 
		{
//			System.out.println("EVENT: mousePressed:");
			if (!simpleLine) {
				pointBuffer = event.getPoint();
			}
		}

		@Override
		public void mouseReleased(MouseEvent event) 
		{
//			System.out.println("EVENT: mouseReleased:");
			
			// In free hand mode the last point. 
			if (!movingMode && !simpleLine && currentShape != null) 
			{
				currentShape.addPoint(event.getPoint());
				currentShape = null;
				repaint();
				createHistorySnapshot();
				return;
			}
			if (movingMode && currentShape != null) {
				createHistorySnapshot();
				repaint();
			}
		}
		
		@Override
		public void mouseClicked(MouseEvent event) 
		{
//			System.out.println("EVENT: mouseClicked:" + event.getButton() );
			// If moving mode on and button number 3 - remove current shape.
			if (movingMode && event.getButton() == 3) {
				if (currentShape != null) {
					shapes.remove(currentShape);
					currentShape = null;
					repaint();
					createHistorySnapshot();
				}
				return;
			}
			if (movingMode) {
				return;
			}
			
			if (currentShape == null) {
				currentShape = new ShapeObject();
				shapes.add(currentShape);
			}
			currentShape.addPoint(event.getPoint());
						
			if (!(simpleLine = !simpleLine)) {
				currentShape = null;
				createHistorySnapshot();
			}
			repaint();
		}
	}
}
