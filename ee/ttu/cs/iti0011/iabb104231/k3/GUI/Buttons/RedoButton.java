package ee.ttu.cs.iti0011.iabb104231.k3.GUI.Buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import ee.ttu.cs.iti0011.iabb104231.k3.GUI.PaintingPanel;


public class RedoButton extends JButton implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -710603774733139643L;
	private PaintingPanel panel;
	
	public RedoButton(PaintingPanel panel)
	{
		this();
		this.panel = panel;
		addActionListener(this);
	}
	public RedoButton()
	{
		super("Redo");
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		System.out.println("EVENT: " + panel);
		panel.redo();
	}	
}
