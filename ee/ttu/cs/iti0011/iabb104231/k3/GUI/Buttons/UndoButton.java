package ee.ttu.cs.iti0011.iabb104231.k3.GUI.Buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import ee.ttu.cs.iti0011.iabb104231.k3.GUI.PaintingPanel;


public class UndoButton extends JButton implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8555665560949680891L;
	
	private PaintingPanel panel;
	
	public UndoButton(PaintingPanel panel)
	{
		this();
		this.panel = panel;
		addActionListener(this);
	}
	public UndoButton()
	{
		super("Undo");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		System.out.println("EVENT: " + e.getClass());
		panel.undo();
	}
}
