package ee.ttu.cs.iti0011.iabb104231.k3.GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class ShapeObject 
{
	
	/**
	 * Shape points.
	 */
	private List<Point> points = new ArrayList<Point>();
	
	/**
	 * Shape color
	 */
	private Color color = Color.BLACK;	

	/**
	 * Set shape color
	 * @param newColor
	 */
	public void setColor(Color newColor)
	{
		color = newColor;
	}
	
	/**
	 * Add new point to the list of shape.
	 * @param newPoint
	 */
	public void addPoint(Point newPoint)
	{
		points.add(newPoint);
	}
	
	/**
	 * Move given shape to given coordinate
	 * @param x
	 * @param y
	 */
	public void move(Point coor)
	{
		if (points.size() == 0) return;
		
//		if ()
		
		Iterator<Point> iterator = points.iterator();		
		while(iterator.hasNext())
		{
			Point p = (Point)iterator.next();
			p.setLocation(p.x, p.y);
		}
	}
	
	/**
	 * Clone shape
	 */
	public ShapeObject clone()
	{
		ShapeObject cloneObject = new ShapeObject();
		
		if (points.size() == 0) return cloneObject;
		
		Iterator<Point> iterator = points.iterator();		
		while(iterator.hasNext())
		{
			cloneObject.addPoint((Point)iterator.next().clone());
		}
		return cloneObject;
	}
	/**
	 * Move given shape to sertain distance to +x, +y.
	 * @param x
	 * @param y
	 */
	public void move(double x, double y)
	{
		if (points.size() == 0) return;
				
		Iterator<Point> iterator = points.iterator();		
		while(iterator.hasNext())
		{
			Point p = (Point)iterator.next();
			p.setLocation(p.x + x, p.y + y);
		}
	}
	
	
	public double distanceToa(Point to)
	{
		double closest = 99999999999D;
		if (points.size() == 0) return closest;
		
		Iterator<Point> iterator = points.iterator();
		Point p1 = (Point)points.get(0);
		double current;
		while(iterator.hasNext())
		{
			Point p2 = (Point)iterator.next();
			
			double normalLength = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
		    if ( (current = Math.abs( (to.x-p1.x) * (p2.y-p1.y) - (to.y-p1.y) * (p2.x-p1.x)) / normalLength) < closest)
		    	closest = current;
		}
		
		return closest;
	}
	
	/**
	 * Calculate distance from given point to the shape.
	 * @param to
	 * @return
	 */
	public double distanceTo(Point to)
	{
		double closest = 100000000000D;
		if (points.size() == 0) {
			System.out.println("SHAPE NO POITS");
			return closest;
		}
		
		Iterator<Point> iterator = points.iterator();
//		Point p1 = (Point)points.get(0);
		
		while(iterator.hasNext()) 
		{
			double current;
			Point p2 = (Point)iterator.next();
			current = p2.distance(to.getX(), to.getY());
			if (closest > current) closest = current;
		}
		
		return closest;
		
//		while(iterator.hasNext())
//		{
//			Point p2 = (Point)iterator.next();
//			Line2D.Double line = new Line2D.Double(p1.x, p1.y, p2.x, p2.y);
//			current = line.ptLineDist(new Point2D.Double(to.x, to.y));
//			if (current < closest) closest = current;
//			
////			double normalLength = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
////		    if ( (current = Math.abs( (to.x-p1.x) * (p2.y-p1.y) - (to.y-p1.y) * (p2.x-p1.x)) / normalLength) < closest)
////		    	closest = current;
//		}
//		System.out.println("CLOSESS: " + closest);
//		return closest;
	}
	
	/**
	 * Draw given shape on graphics.
	 * @param g
	 */
	public void draw(Graphics g)
	{
		if (points.size() == 0) return;
		
		g.setColor(color);
		Iterator<Point> iterator = points.iterator();
		
		Point p1 = (Point)points.get(0);
		
		while(iterator.hasNext())
		{
			Point p2 = (Point)iterator.next();
			g.drawLine(p1.x, p1.y, p2.x, p2.y);
			p1 = p2;
		}
	}
}
