package ee.ttu.cs.iti0011.iabb104231.k3.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;

import ee.ttu.cs.iti0011.iabb104231.k3.GUI.Buttons.RedoButton;
import ee.ttu.cs.iti0011.iabb104231.k3.GUI.Buttons.UndoButton;


public class MainWindow extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5553882296779593851L;
	
	PaintingPanel panel;
	
	public MainWindow(String title)
	{
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);
        setMinimumSize(new Dimension(300, 300));
        
        // Set layout to border. For future, if we want to add left or bottom panes.
        setLayout(new BorderLayout());
        
        // Create panels for buttons and for painting.
        JPanel buttonsPanel = new JPanel();
        PaintingPanel paintingPanel = new PaintingPanel();
        
        UndoButton undoButton = new UndoButton(paintingPanel);        
        RedoButton redoButton = new RedoButton(paintingPanel);
        
        buttonsPanel.add(undoButton);
        buttonsPanel.add(redoButton);
        
        getContentPane().add(buttonsPanel, BorderLayout.PAGE_START);
        getContentPane().add(paintingPanel, BorderLayout.CENTER);
        
        panel = paintingPanel;
        pack();
        setLocationRelativeTo(null);
        setVisible(true);  // make window visible
	}
	
	public MainWindow()
	{
	}

}
